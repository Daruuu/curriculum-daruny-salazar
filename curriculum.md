
# Curriculum 

![fotoPerfil](link)

**Nombre:** Daruny Azareel Salazar Cardoza
**Dirección:** 08032, Barcelona
**Correo electrónico:** thedaruny16@gmail.com
**Número de teléfono:** (+34) 654168633
**LinkedIn:** ![LinkedIn](https://www.linkedin.com/in/daruny)
**Género:** Masculino

## Actualmente

Terminando un bootcamp de Java & SQl y continuando con grado superior DAM.
Con mucha curiosidad por seguir aprendiendo todo lo que tiene que ver con el backend developer con Java.


## Estudios
[ 17/07/2021 – 04/09/2021 ] Instalador de Fibra Optica 
    Orange
Población: Barcelona
Principales actividades y responsabilidades:
    Instalación de líneas ADSL o Fibra óptica.
    Instalación de routers y activación de líneas

## Formación complementaria
1. Lenguajes de Programacion: Java
2. Bases de datos: SQL, MongoDB
3. Desarollo Web: HTML, CSS
4. Frameworks: Bootstrap, Springboot(basico)
5. Otros: Linux, bash, Fish, Git, Docker, Vim

## Idiomas
Idioma | COMPRENSIÓN AUDITIVA | COMPRENSIÓN LECTORA | EXPRESIÓN ESCRITA | INTERACCIÓN ORAL 
---|---|---|---|---
Español | lengua materna
Catalan | B2 | B2 | B1 | B1
Ingles  | B1 | B1 | A2 | A2


## Soft-Skills
**Capacidades Digitales:** Organizacion y Puntualidad | Responsabilidad en el trabajo | Sinergia | Autodidacta y
continua formación en temas que me apasionan

## Hobbies
leer libros , hacer deporte. 
